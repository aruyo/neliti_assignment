expression = []                                                     #global expression
def f(numbers, target):                                             
    global expression           
    ways = expression                       
    get_exprs(numbers, target)          
    for expr in ways:                                               #loop expression
        expression = []                                             #set expression to empty
        return "'"+f'{target}'+" = "+expr+"'"                       #set return with format 'target = expression'

def get_exprs(numbers, target,pos=0, cur_val=0, expr=''):
    if(cur_val == target) :                                         #check current value same with target
        set_expr(expr)                                              #set expression
        return
    
    if(pos == len(numbers)):                                        #check if position same with length of numbers
        if(cur_val == target):                                      #set expression
            set_expr(expr)
        return

    cur_part = numbers[pos]                                         #set current part with numbers[position]

    if(pos == 0):
        get_exprs( numbers, target, pos + 1, cur_part, str(cur_part))  #init expression
    else:
        get_exprs( numbers, target, pos + 1, cur_val + cur_part, expr  + ' + ' + str(cur_part))  
        get_exprs( numbers, target, pos + 1, cur_val - cur_part, expr  + ' - ' + str(cur_part))

def set_expr(i):
    global expression
    expression.append(i)

print(f([1, 2, 3, 4, 5], 9))
print(f([2, 5, 60, -5, 3], 69))
print(f([2, 5, 10],50))