# Weather App

## Description

A web application to show weather data in a specific location.

## Technology stack

1. Django

## Instruction 

1. if using virtualenv :

    ```
    pip install virtualenv
    virtualenv venv
    ./venv/Scripts/activate
    ```

2. install requirements

    ```
    pip install -r requirements.txt
    ```

3. navigate to project directory

    ```
    cd teliti_project
    ```

4. migrate database

    ```
    python manage.py migrate
    ```

5. run server

    ```
    python manage.py runserver
    ```

6. open web browser

    ```
    open http://127.0.0.1:8000/weather/show
    ```