from datetime import date
import calendar
from django import template
from datetime import date 
import requests

register = template.Library()

# Endpoint for parse weather code to description in english
# https://api.met.no/weatherapi/weathericon/2.0/legends

@register.filter(name='concat')
def concat(value, arg):
    return str(value) + str(arg)

@register.filter(name='formatDatePretty')
def formatDatePretty(str_date):
    x = date.fromisoformat(str_date)
    return(x.strftime("%b %d %Y"))

@register.filter(name='formatDateSimple')
def formateDateSimple(str_date):
    x = date.fromisoformat(str_date)
    return(x.strftime("%m/%d"))

@register.filter(name='parseToImagePath')
def parseToImagePath(code):
    return "/weather_app/weathericon/png/" + code +".png"

@register.filter(name='formatWeatherCode')
def formatWeatherCode(code):
    if code:
        url = "https://api.met.no/weatherapi/weathericon/2.0/legends"
        headers = {"User-Agent": "Radhin (Windows NT 10.0; Win64;"}
        response=requests.get(url, headers=headers).json()    

        weather_code  = code.split("_")[0]
        return response[weather_code]["desc_en"]
    else:
        return ""

@register.filter(name='formatAirTemperatureUnit')
def formatAirTemperatureUnit(temp):
    number, unit = temp.split(" ")
    if unit == "celsius":
        return number + "°C"
    return temp

@register.filter(name='formatTime')
def formatTime(time):
    h, m, s = time.split(':')
    return h + ":" + m

@register.filter(name='dayName')
def dayName(str_date):
    my_date = date.fromisoformat(str_date)
    return calendar.day_name[my_date.weekday()]  

@register.filter(name='formatWindDirection')
def formatWindDirection(wind_direction):
    number, unit = wind_direction.split(" ")
    return number + "°"