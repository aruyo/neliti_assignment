from django.shortcuts import render
from django.http import Http404
import requests
import datetime 

# Create your views here.
def index(request):

    # Dummy Data for current_weather and list of next weather
    currentWeather = {}
    parsedData = []
    weatherData = {}

    weatherData['date']                      = "2021-06-26"
    weatherData['time']                      = "17:00:00"
    weatherData['air_pressure_at_sea_level'] = "1017.5 hPa"
    weatherData['air_temperature']           = "24.6 celcius"
    weatherData['cloud_area_fraction']       = "78.9 %"
    weatherData['relative_humidity']         = "48.7 %"
    weatherData['wind_from_direction']       = "141.3 degrees"
    weatherData['wind_speed']                = "3.1 m/s"
    weatherData['next_1_hours']              = "fair_day"
    
    currentWeather = weatherData.copy()  
    for i in range(10):
        parsedData.append(weatherData)
    return render(request, 'weather_app/forecast.html', {'current_weather': currentWeather, 'data': parsedData})

def show(request):
    # try :
    #     latitude, longitude = float(latitude), float(longitude)
    # except ValueError:
    #     raise Http404("Poll does not exist")
    url = 'https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=40&lon=-0.25'    #url api for weather data
    headers = {"User-Agent": "Radhin (Windows NT 10.0; Win64;"}
    response=requests.get(url, headers=headers).json()                                     #get request to api and parse to json
    parsedData = []                                                                        #init parsedData
    today = datetime.datetime.today()
    hours = today.hour
    today = today.replace(hour=hours-1)
    currentWeather = {}                                                                     #set current weather as dictionary
    count=0
    for i in response['properties']['timeseries']:                                          #loop api response
        date=datetime.datetime.strptime(i['time'], "%Y-%m-%dT%H:%M:%SZ")                    #parse datetime
        if (date >= today):                                                                 #check if date is bigger or equals with today
            weatherData = {}                                                                #init and passing data to weatherData dictionary
            weatherData['date'] = str(i['time'])[0:10]
            weatherData['time'] = str(i['time'])[11:19]
            weatherData['air_pressure_at_sea_level'] = str(i['data']['instant']['details']['air_pressure_at_sea_level']) + ' ' + response['properties']['meta']['units']['air_pressure_at_sea_level']
            weatherData['air_temperature'] = str(i['data']['instant']['details']['air_temperature']) + ' ' + response['properties']['meta']['units']['air_temperature']
            weatherData['cloud_area_fraction'] = str(i['data']['instant']['details']['cloud_area_fraction']) + ' ' + response['properties']['meta']['units']['cloud_area_fraction']
            weatherData['relative_humidity'] = str(i['data']['instant']['details']['relative_humidity']) + ' ' + response['properties']['meta']['units']['relative_humidity']
            weatherData['wind_from_direction'] = str(i['data']['instant']['details']['wind_from_direction']) + ' ' + response['properties']['meta']['units']['wind_from_direction']
            weatherData['wind_speed'] = str(i['data']['instant']['details']['wind_speed']) + ' ' + response['properties']['meta']['units']['wind_speed']             
            weatherData['next_1_hours'] = ''
            if (str(today)[0:10] == weatherData['date']):                                                #check weatherdata today
                weatherData['next_1_hours'] = str(i['data']['next_1_hours']['summary']['symbol_code'])   #set weaterdata for next 1 hour
                if (count == 0):                                                                         #chcek if count = 0
                    currentWeather = weatherData.copy()                                                  #copy weatherdata with given condition to current weather
                    count = 1                                                                            #set count to 1
                else:
                    parsedData.append(weatherData)                                                       #append weatherData to parsedData
            
    return render(request, 'weather_app/forecast.html', {'current_weather': currentWeather, 'data': parsedData})   #parse to forecast html
    # return render(request, 'weather_app/show.htm', {'current_weather':currentWeather, 'data':parsedData})
