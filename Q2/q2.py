from journal.models import Journal  #import jornal models (adjust this with your package)

def run():
    print(get_journal_statistics())

def get_journal_statistics():
    summary = Journal.objects.all()                         #fetch all journal
    response = {}
    for entry in summary:                                   #loop journal
        counter_view = 0
        counter_download = 0
        for publication in entry.publication_set.all():     #loop publication in journal
            for hits in publication.hit_set.all():          #loop hits in publication
                if(hits.action == "PV"):                    #check if hits action is 'PAGEVIEW'
                    counter_view += 1                       #add counter view +1
                else :                                      #check if hits action is not 'PAGEVIEW'
                    counter_download += 1                   #add counver download +1
        response[entry.id] = {                              #create dictionary with {journal_id->{counter_view, counter_download}}
            counter_view,
            counter_download
        }
    return response                                         #return dictionary