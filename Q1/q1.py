import urllib.request, urllib.error, urllib.parse
from bs4 import BeautifulSoup

url = 'http://www.oldbaileyonline.org/browse.jsp?id=t17800628-33&div=t17800628-33'

def word_frequencies(url):
    response = urllib.request.urlopen(url)                  #open url 
    webContent = response.read()                            #read response
    f = open('parse.html','wb')                             #saveit to parse.html
    f.write(webContent)
    f.close
    cleanText = BeautifulSoup(webContent, "lxml").text      #remove html tag
    dicts = {}

    splitCleanText = cleanText.split()                      #split text by space
    for word in splitCleanText:                             #loop split text
        check = checkKey(dicts, word)                       #check word in dictionary
        if check :                                          #if true
            dicts[word] += 1                                #add counter +1
        else :                                              #else
            dicts[word] = 1                                 #add counter 1
    
    return dicts                                            #return dictionary


def checkKey(dict, key):
      
    if key in dict:
        return True
    else:
        return False

word_frequencies(url)
